package com.bellintegrator.springcontext.model;

import com.bellintegrator.springcontext.aspect.Delay;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalTime;

@Accessors(fluent = true, chain = true)
@Getter
@Setter
@AllArgsConstructor
public class User {

    private Long id;
    private String name;
    private String email;

    public void whatTimeIsIt() {
        System.out.println("Который час? " + LocalTime.now());
    }

    public void speak() {
        System.out.println(name + " говорит: " + LocalTime.now());
    }
    @Delay(2000)
    public void speakStuttering() {
        System.out.println(name + " говорит, но не сразу: " + LocalTime.now());
    }

}

package com.bellintegrator.springcontext.config;

import com.bellintegrator.springcontext.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Random;

@Configuration
public class UserConfig {

    @Bean
    @Scope("prototype")
    public User getUser() {
        return new User(new Random().nextLong(), "Иван", "ivan@bellintegrator.com");
    }
}

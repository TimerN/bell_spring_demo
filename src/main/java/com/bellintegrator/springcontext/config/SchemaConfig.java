package com.bellintegrator.springcontext.config;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;


@Log4j2
@Configuration
public class SchemaConfig {
    private final String CREATE_BANK_SCHEMA = "create schema if not exists bank";

    @Value("${db.create.schema.enabled}")
    private boolean createSchemaEnabled;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public SchemaConfig() {
        log.info("[SchemaConfig] SchemaConfig bean creation start");
        if (createSchemaEnabled) {
            jdbcTemplate.update(CREATE_BANK_SCHEMA);
        }
        log.info("[SchemaConfig] SchemaConfig bean creation end");
    }

    @PostConstruct
    private void init() {
        log.info("[SchemaConfig] createSchemaEnabled {}", createSchemaEnabled);
    }

    @PreDestroy
    private void destroy() {
        log.info("[SchemaConfig] createSchemaEnabled destroyed");
    }

}
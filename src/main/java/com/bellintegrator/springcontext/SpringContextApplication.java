package com.bellintegrator.springcontext;

import com.bellintegrator.springcontext.config.ApplicationConfig;
import com.bellintegrator.springcontext.model.User;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@Log4j2
@SpringBootApplication
public class SpringContextApplication {
    private static ApplicationConfig applicationConfig;

    @Autowired
    private ApplicationConfig initApplicationConfig;

    @PostConstruct
    public void init() {
        applicationConfig = initApplicationConfig;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringContextApplication.class, args);

        User ivan = applicationConfig.getApplicationContext().getBean(User.class);
        log.info("[SpringContextApplication] ivan id: {}", ivan.id());

        User oleg = applicationConfig.getApplicationContext().getBean(User.class);
        oleg.id(new Random().nextLong()).name("Олег").email("oleg@bellintegrator.com");
        log.info("[SpringContextApplication] oleg id: {}", oleg.id());
//
//        log.info("[SpringDemoApplication] ivan equals oleg ?: {}", ivan.equals(oleg));
//        log.info("[SpringDemoApplication] ivan id: {}", ivan.id());
//        log.info("[SpringDemoApplication] oleg id: {}", oleg.id());
        ivan.whatTimeIsIt();
        oleg.speak();
        oleg.whatTimeIsIt();
        ivan.speakStuttering();
    }

}

package com.bellintegrator.springcontext.aspect;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Log4j2
@Component
@Aspect
public class MethodDelayer {

    @Pointcut("@annotation(com.bellintegrator.springcontext.aspect.Delay)")
    public void delayAnnotation() {}

    @Around("delayAnnotation()")
    public void delay(ProceedingJoinPoint point) throws Throwable {

        Method method = ((MethodSignature) point.getSignature()).getMethod();
        log.info("Начало метода " + method.getName());
        Delay annotation = method.getAnnotation(Delay.class);
        long sleepMillis = annotation.value();
        Thread.sleep(sleepMillis);
        point.proceed();
        log.info("Конец метода " + method.getName());
    }

}
